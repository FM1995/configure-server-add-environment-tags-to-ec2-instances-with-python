# Configure Server: Add Environment Tags to EC2 Instances with Python

#### Project Outline

Lets say we have a set of servers in the Paris Region labelled as Production and a set of servers in the Frankfurt region set as Development

For simplicity we will create 2 instances in Paris and 2 in Frankfurt

#### Lets get started

Adding new file add-env-tags.py and renaming the previous file as ec2-status-checks

![Image 1](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image1.png)

Can pick out the ‘Reservations’ as seen in the response syntax to get the instances in the defined region

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_instances.html

![Image 2](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image2.png)

Can then append then get the Instances and append the Instance Id’s to a list

![Image 3](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image3.png)

Can then create the tag and attach to the paris ec2

The below documentation can be used to configure it

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/instance/index.html

Can see it requires a resource

![Image 4](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image4.png)

Can proceed to create the tag

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/instance/create_tags.html

![Image 5](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image5.png)

Can define the resource and the response for paris

![Image 6](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image6.png)


Next we can do it for Frankfurt, which is a duplication for paris

![Image 7](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image7.png)

Despite not having the ec2 instances in the mentioned regions

Can quickly create instances in each region

![Image 8](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image8.png)

And have now created instances in the resprective regions

![Image 9](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image9.png)

Lets execute the code and can see it go executed successfully

![Image 10](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image10.png)


And can see they go tagged in each region

![Image 11](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image11.png)


And the same in Frankfurt

![Image 12](https://gitlab.com/FM1995/configure-server-add-environment-tags-to-ec2-instances-with-python/-/raw/main/Images/Image12.png)






